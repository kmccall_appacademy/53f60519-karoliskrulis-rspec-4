class Friend
  # TODO: your code goes here!
  def greeting(person = nil)
    return "Hello!" if person.nil?
    "Hello, #{person}!"
  end
end
