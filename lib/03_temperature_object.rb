class Temperature
  # TODO: your code goes here!
  def initialize(option)
    option[:f] ? self.fahrenheit = option[:f] : self.celsius = option[:c]
  end
  def fahrenheit=(temp)
      @temp = self.class.ftoc(temp)
  end
  def celsius=(temp)
      @temp = temp
  end
  def self.ftoc(temp)
      (temp - 32) * 5 / 9.0
  end
  def self.ctof(temp)
      temp * 9 / 5.0 + 32
  end
  def in_fahrenheit
      self.class.ctof(@temp)
  end
  def in_celsius
      @temp
  end

  # factory methods
  def self.from_fahrenheit(value)
    Temperature.new(:f => value)
  end


  def self.from_celsius(value)
    Temperature.new(:c => value)
  end
end

# sub classes
  class Fahrenheit < Temperature
    def initialize(temp)
      self.fahrenheit = temp
    end
  end

  class Celsius < Temperature
    def initialize(temp)
      self.celsius = temp
    end
  end
