class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def padded(digits)
    p "%02d" % digits
  end

  # time_string can't access seconds_conversion variables
  # need to create a method for hh, mm, ss separately OR global variable
  # def seconds_conversion
  #   hh = seconds / 3600
  #   mm = (seconds % 3600) / 60
  #   ss = seconds % 60
  # end
  def hh
    seconds / 3600
  end

  def mm
    (seconds % 3600) / 60
  end

  def ss
    seconds % 60
  end

  def time_string
    "#{padded(hh)}:#{padded(mm)}:#{padded(ss)}"
  end

end
