class Book
  # TODO: your code goes here!

  SMALL_WORD = ["a", "the", "and", "an", "of", "in"]

  attr_reader :title

  def title=(title)
    # don't entirely understand useage of title=(title)
    words = title.split(' ').map(&:downcase)

    new_words = words.map.with_index do |w, i|
      if SMALL_WORD.include?(w) && i != 0
        w
      else
        w.capitalize
      end
    end

    @title = new_words.join(' ')
  end
end
